#!/bin/bash

function gnirehtet(){
	gn_red='\033[0;31m' # Red
	gn_nc='\033[0m' # No Color
	echo -e "${gn_red}custom command: check ~/.zshrc for info${gn_nc}"
	A=$(pwd)
	cd /Users/rahul/extra-utils/gnirehtet-java
	./gnirehtet $1
	cd "${A}"
}
