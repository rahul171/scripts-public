#!/bin/bash

ts=$1
tsc ${ts}

if [ $? -ne 0 ]; then
	exit 0;
fi

js="${ts%???}.js"
node ${js}
